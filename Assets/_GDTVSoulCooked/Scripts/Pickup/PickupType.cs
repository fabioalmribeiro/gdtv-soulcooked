using UnityEngine;

namespace GDTVSoulCooked.Pickup
{
    [CreateAssetMenu(menuName = "GDTVSoulCooked/Pickups/Pickup Type")]
    public class PickupType : ScriptableObject
    {
        [SerializeField] private Sprite _icon;

        public Sprite GetIcon()
        {
            return _icon;
        }
    }
}
