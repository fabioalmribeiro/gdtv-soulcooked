using UnityEngine;

namespace GDTVSoulCooked.Pickup
{
    public class Stand : MonoBehaviour
    {
        [SerializeField] private PickupType _pickupType;

        [Header("References")]
        [SerializeField] private SpriteRenderer _icon;

        public PickupType GetPickupType()
        {
            return _pickupType;
        }

        private void Start()
        {
            if (!_pickupType) return;

            _icon.sprite = _pickupType.GetIcon();
        }
    }
}
