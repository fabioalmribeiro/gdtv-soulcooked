using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections;
using GDTVSoulCooked.Pickup;
using GDTVSoulCooked.Level;
using GDTVSoulCooked.UI;
using GDTVSoulCooked.Request;

namespace GDTVSoulCooked.Control
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Movement")]
        [SerializeField] private float _timeToMove = 0.2f;
        [SerializeField] private LayerMask _obstaclesLayer;
        [SerializeField] private int _standsLayer;
        [SerializeField] private int _exitLayer;
        [SerializeField] private AudioClip _moveSFX;
        private Vector2 _axisMove;
        private bool _isMoving = false;

        [Header("Pickup")]
        [SerializeField] private SpriteRenderer _pickup;
        [SerializeField] private AudioClip _pickupSFX;
        private PickupType _currentPickup;
        private Collider2D _interactCollider;
        private bool _holdingInteractBtn = false;

        private AudioSource _audioSource;

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        private void Update()
        {
            HandleMovement();
            HandleInteract();
        }

        private void HandleMovement()
        {
            if (Mathf.Abs(_axisMove.x) == 1f && !_isMoving)
            {
                Vector3 direction = new Vector3(_axisMove.x, 0f, 0f);

                if (!CanMove(direction)) return;

                StartCoroutine(MovePlayer(direction));
            }
            else if (Mathf.Abs(_axisMove.y) == 1f && !_isMoving)
            {
                Vector3 direction = new Vector3(0f, _axisMove.y, 0f);

                if (!CanMove(direction)) return;

                StartCoroutine(MovePlayer(direction));
            }
        }

        private void HandleInteract()
        {
            if (_interactCollider == null) return;
            if (!_holdingInteractBtn) return;

            _holdingInteractBtn = false;

            if (_interactCollider.TryGetComponent<Stand>(out Stand stand))
            {
                _currentPickup = stand.GetPickupType();
                _pickup.sprite = stand.GetPickupType()?.GetIcon();
            }
            else
            {
                RequestsManager.Instance.FullfillObjective(_currentPickup);
                _currentPickup = null;
                _pickup.sprite = null;
            }

            _audioSource.PlayOneShot(_pickupSFX, 0.5f);
        }

        private bool CanMove(Vector3 direction)
        {
            Collider2D collider = Physics2D.OverlapCircle(transform.position + direction, 0.2f, _obstaclesLayer);
            if (collider != null)
            {
                return collider.isTrigger;
            }

            return true;
        }

        private IEnumerator MovePlayer(Vector3 direction)
        {
            _isMoving = true;

            float elapsedTime = 0f;

            Vector3 originPos = transform.position;
            Vector3 targetPos = originPos + direction;

            while (elapsedTime < _timeToMove)
            {
                transform.position = Vector3.Lerp(originPos, targetPos, (elapsedTime / _timeToMove));
                elapsedTime += Time.deltaTime;

                yield return null;
            }

            transform.position = targetPos;

            _audioSource.PlayOneShot(_moveSFX, 0.5f);

            _isMoving = false;

            foreach (var tile in TilesManager.Instance.RemovedTiles)
            {
                if (transform.position.Equals(tile.transform.position))
                {
                    GameOverManager.Instance.OnDeath();
                    break;
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.layer != _standsLayer && collider.gameObject.layer != _exitLayer) return;
            if (!collider.isTrigger) return;

            _interactCollider = collider;
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            if (!collider.Equals(_interactCollider)) return;

            _interactCollider = null;
        }

        public void OnTryMove(InputAction.CallbackContext context)
        {
            _axisMove = context.ReadValue<Vector2>();
        }

        public void OnTryInteract(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                _holdingInteractBtn = true;
            }
            else if (context.phase == InputActionPhase.Canceled)
            {
                _holdingInteractBtn = false;
            }
        }
    }
}
