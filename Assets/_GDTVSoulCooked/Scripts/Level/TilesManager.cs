using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GDTVSoulCooked.Level
{
    public class TilesManager : MonoBehaviour
    {
        public static TilesManager Instance { get; private set; }

        [Header("Settings")]
        [SerializeField] private int _maxRemovedTiles = 5;
        [SerializeField] private float _difficultyIncreaseDelay = 5f;
        [SerializeField] private float _delayBetweenSpawningTiles = 0.2f;
        private float _spawningTilesTimer = 0f;

        public readonly Queue<FloorTile> RemovedTiles = new Queue<FloorTile>();

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private IEnumerator Start()
        {
            while (PlayerPrefs.GetInt("_hardmode") == 1)
            {
                yield return new WaitForSeconds(_difficultyIncreaseDelay);

                _maxRemovedTiles++;
            }
        }

        private void Update()
        {
            if (RemovedTiles.Count >= _maxRemovedTiles)
            {
                _spawningTilesTimer += Time.deltaTime;

                if (_spawningTilesTimer >= _delayBetweenSpawningTiles)
                {
                    _spawningTilesTimer = 0f;

                    FloorTile tile = RemovedTiles.Dequeue();
                    tile.gameObject.SetActive(true);
                }
            }
        }
    }
}
