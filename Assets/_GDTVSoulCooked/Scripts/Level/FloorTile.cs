using UnityEngine;
using System.Collections;

namespace GDTVSoulCooked.Level
{
    public class FloorTile : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private float _timeToShrink = 0.5f;

        private void OnEnable()
        {
            if (GetComponent<Collider2D>().enabled) return;

            StopAllCoroutines();
            StartCoroutine(ExpandTile());
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            if (collider.CompareTag("Player"))
            {
                StopAllCoroutines();
                StartCoroutine(ShrinkTile());
            }
        }

        private IEnumerator ShrinkTile()
        {
            GetComponent<Collider2D>().enabled = false;

            float elapsedTime = 0f;

            while (elapsedTime < _timeToShrink)
            {
                transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, (elapsedTime / _timeToShrink));
                elapsedTime += Time.deltaTime;

                yield return null;
            }

            transform.localScale = Vector3.zero;

            gameObject.SetActive(false);

            TilesManager.Instance.RemovedTiles.Enqueue(this);
        }

        private IEnumerator ExpandTile()
        {
            GetComponent<Collider2D>().enabled = true;

            float elapsedTime = 0f;

            while (elapsedTime < _timeToShrink)
            {
                transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, (elapsedTime / _timeToShrink));
                elapsedTime += Time.deltaTime;

                yield return null;
            }

            transform.localScale = Vector3.one;
        }
    }
}
