using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace GDTVSoulCooked.UI
{
    public class MainMenuManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _highscoreLabel;
        [SerializeField] private TextMeshProUGUI _hardModeLabel;

        private void Start()
        {
            _highscoreLabel.text = $"Highscore: { PlayerPrefs.GetInt("_highscore").ToString("00000") }";

            UpdateHardModeLabel();
        }

        private void UpdateHardModeLabel()
        {
            string hardModeStatus = PlayerPrefs.GetInt("_hardmode") == 1 ? "ON" : "OFF";
            _hardModeLabel.text = $"Hard Mode: { hardModeStatus }";
        }

        public void PlayGame()
        {
            SceneManager.LoadScene(PlayerPrefs.GetInt("_highscore") == 0 ? 1 : 2);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void TriggerHardMode()
        {
            PlayerPrefs.SetInt("_hardmode", PlayerPrefs.GetInt("_hardmode") == 1 ? 0 : 1);

            UpdateHardModeLabel();
        }
    }
}
