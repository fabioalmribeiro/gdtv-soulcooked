using UnityEngine;
using GDTVSoulCooked.Request;

namespace GDTVSoulCooked.UI
{
    public class RequestsList : MonoBehaviour
    {
        [SerializeField] private RequestCard _cardPrefab;

        private void Start()
        {
            RequestsManager.Instance.OnRequestCreated += RequestsManager_OnRequestCreation;
        }

        private void OnDisable()
        {
            RequestsManager.Instance.OnRequestCreated -= RequestsManager_OnRequestCreation;
        }

        private void RequestsManager_OnRequestCreation(Request.Request request)
        {
            RequestCard card = Instantiate(_cardPrefab, transform);
            card.SetRequest(request);
        }
    }
}
