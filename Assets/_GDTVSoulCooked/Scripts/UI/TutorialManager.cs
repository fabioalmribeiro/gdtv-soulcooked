using UnityEngine;
using UnityEngine.SceneManagement;

namespace GDTVSoulCooked.UI
{
    public class TutorialManager : MonoBehaviour
    {
        public void PlayGame()
        {
            SceneManager.LoadScene(2);
        }
    }
}
