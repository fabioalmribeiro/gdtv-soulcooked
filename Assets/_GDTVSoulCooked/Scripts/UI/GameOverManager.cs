using UnityEngine;
using UnityEngine.SceneManagement;

namespace GDTVSoulCooked.UI
{
    public class GameOverManager : MonoBehaviour
    {
        public static GameOverManager Instance { get; private set; }

        [SerializeField] private GameObject _canvas;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }

            _canvas.SetActive(false);
        }

        public void OnDeath()
        {
            Time.timeScale = 0f;

            _canvas.SetActive(true);
        }

        public void OnRetry()
        {
            Time.timeScale = 1f;

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void OnExit()
        {
            Time.timeScale = 1f;

            if (PlayerPrefs.GetInt("_highscore") < ScoreDisplay.Score)
            {
                PlayerPrefs.SetInt("_highscore", ScoreDisplay.Score);
            }

            SceneManager.LoadScene(0);
        }
    }
}
