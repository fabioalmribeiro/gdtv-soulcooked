using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GDTVSoulCooked.Request;
using GDTVSoulCooked.Pickup;
using System.Collections.Generic;

namespace GDTVSoulCooked.UI
{
    public class RequestCard : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameLabel;
        [SerializeField] private Image _timerBar;

        [Header("Pickups")]
        [SerializeField] private Transform _pickupsHolder;
        [SerializeField] private Image _iconPrefab;

        private Dictionary<PickupType, Image> _objectives = new Dictionary<PickupType, Image>();
        private Request.Request _request;
        private float _timer = Mathf.Infinity;

        private void OnEnable()
        {
            RequestsManager.Instance.OnObjectiveCompleted += RequestsManager_OnObjectiveCompleted;
        }

        private void OnDisable()
        {
            RequestsManager.Instance.OnObjectiveCompleted -= RequestsManager_OnObjectiveCompleted;
        }

        private void Update()
        {
            if (_request == null) return;
            if (_timer <= 0)
            {
                RequestsManager.Instance.FailRequest(_request);
                GetComponent<Image>().color = Color.red;
                Destroy(gameObject, 0.2f);
                return;
            }

            _timer -= Time.deltaTime;

            _timerBar.fillAmount = (_timer * 100 / _request.Duration) * 0.01f;
        }

        private void RequestsManager_OnObjectiveCompleted(Request.Request request, PickupType pickupType)
        {
            if (request.ID == _request.ID)
            {
                _objectives[pickupType].color = Color.black;

                int completedObjectives = 0;
                foreach (var keyValuePair in _objectives)
                {
                    if (keyValuePair.Value.color == Color.black)
                    {
                        completedObjectives++;
                    }
                }

                if (completedObjectives == _objectives.Count)
                {
                    GetComponent<Image>().color = Color.green;
                    Destroy(gameObject, 0.2f);
                }
            }
        }

        public void SetRequest(Request.Request request)
        {
            _request = request;

            _nameLabel.text = request.Profession.GetDisplayName();
            _timer = request.Duration;

            foreach (var item in request.Profession.GetRecipe())
            {
                Image icon = Instantiate(_iconPrefab, _pickupsHolder);
                icon.sprite = item.GetIcon();
                icon.preserveAspect = true;

                _objectives.Add(item, icon);
            }
        }
    }
}
