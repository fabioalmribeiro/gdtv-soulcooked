using UnityEngine;
using TMPro;

namespace GDTVSoulCooked.UI
{
    public class TimerDisplay : MonoBehaviour
    {
        [SerializeField] private float _roundDuration = 60f;
        private float _timer = 0f;
        private TextMeshProUGUI _textLabel;

        private void Start()
        {
            _textLabel = GetComponent<TextMeshProUGUI>();
            _timer = _roundDuration;
        }

        private void Update()
        {
            if (_timer <= 0f)
            {
                GameOverManager.Instance.OnDeath();
                return;
            }

            _timer -= Time.deltaTime;
            var timerString = _timer.ToString("00.00").Split('.');

            _textLabel.text = $"{ timerString[0] }.<size=60>{ timerString[1] }</size>";
        }
    }
}
