using UnityEngine;
using TMPro;
using GDTVSoulCooked.Request;

namespace GDTVSoulCooked.UI
{
    public class ScoreDisplay : MonoBehaviour
    {
        private TextMeshProUGUI _text;
        public static int Score = 0;

        private void Start()
        {
            _text = GetComponent<TextMeshProUGUI>();
            _text.text = Score.ToString("00000");

            RequestsManager.Instance.OnRequestCompleted += RequestsManager_OnRequestCompleted;
        }

        private void OnDisable()
        {
            RequestsManager.Instance.OnRequestCompleted -= RequestsManager_OnRequestCompleted;
        }

        private void RequestsManager_OnRequestCompleted()
        {
            Score += 100;

            _text.text = Score.ToString("00000");
        }
    }
}
