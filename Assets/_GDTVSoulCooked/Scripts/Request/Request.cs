using UnityEngine;
using System.Collections.Generic;
using GDTVSoulCooked.Pickup;

namespace GDTVSoulCooked.Request
{
    public class Request
    {
        public string ID;
        public float Duration;
        public Profession Profession;
        public Dictionary<PickupType, bool> Objectives;
    }
}
