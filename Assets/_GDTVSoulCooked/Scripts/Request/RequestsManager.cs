using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GDTVSoulCooked.Pickup;
using Random = UnityEngine.Random;

namespace GDTVSoulCooked.Request
{
    public class RequestsManager : MonoBehaviour
    {
        public static RequestsManager Instance { get; private set; }

        public Action<Request> OnRequestCreated;
        public Action<Request, PickupType> OnObjectiveCompleted;
        public Action OnRequestCompleted;

        [SerializeField] private List<Profession> _availableProfessions;

        [Header("Queue Settings")]
        [SerializeField] private int _maxConcurrentRequests = 3;
        [SerializeField] private float _delayBetweenRequests = 5f;
        [SerializeField] private float _minToleranceTimer = 15f;
        [SerializeField] private float _maxToleranceTimer = 30f;

        private List<Request> _queuedRequests = new List<Request>();

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private IEnumerator Start()
        {
            while (true)
            {
                if (_queuedRequests.Count != _maxConcurrentRequests)
                {
                    var prof = _availableProfessions[Random.Range(0, _availableProfessions.Count)];
                    var objs = new Dictionary<PickupType, bool>();

                    foreach (var item in prof.GetRecipe())
                    {
                        objs.Add(item, false);
                    }

                    Request newRequest = new Request
                    {
                        ID = DateTime.Now.ToString(),
                        Duration = Random.Range(_minToleranceTimer, _maxToleranceTimer + 1),
                        Profession = prof,
                        Objectives = objs
                    };

                    _queuedRequests.Add(newRequest);

                    OnRequestCreated?.Invoke(newRequest);
                }

                yield return new WaitForSeconds(_delayBetweenRequests);
            }
        }

        public void FailRequest(Request request)
        {
            _queuedRequests.Remove(request);
        }

        public void FullfillObjective(PickupType pickupType)
        {
            if (pickupType == null) return;

            var request = _queuedRequests[0];
            var objectives = request.Objectives;
            if (objectives.ContainsKey(pickupType))
            {
                objectives[pickupType] = true;
                OnObjectiveCompleted?.Invoke(request, pickupType);
            }

            int totalCompleted = 0;
            foreach (var keyValuePair in objectives)
            {
                if (keyValuePair.Value == true) totalCompleted++;
            }

            if (objectives.Count == totalCompleted)
            {
                _queuedRequests.Remove(_queuedRequests[0]);
                OnRequestCompleted?.Invoke();
            }
        }
    }
}
