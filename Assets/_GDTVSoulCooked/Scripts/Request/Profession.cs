using UnityEngine;
using System.Collections.Generic;
using GDTVSoulCooked.Pickup;

namespace GDTVSoulCooked.Request
{
    [CreateAssetMenu(menuName = "GDTVSoulCooked/Requests/Profession")]
    public class Profession : ScriptableObject
    {
        [SerializeField] private string _displayName;
        [SerializeField] private List<PickupType> _recipe;

        public string GetDisplayName() => _displayName;

        public List<PickupType> GetRecipe() => _recipe;
    }
}
